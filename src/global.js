export const apiKey = "cateno_hm_dev"; // cateno_hm_dev, cateno_homol, cateno
export const baseApiUrl = "https://service2.funifier.com";
export const secret = "5c6410d5b6fc5f034d8a8e29";
export const token =
  "Basic " + Buffer.from(`${apiKey}:${secret}`).toString("base64");

export default { apiKey, baseApiUrl, secret, token };
