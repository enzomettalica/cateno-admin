import Vue from "vue";
import Router from "vue-router";

import { isAuth } from "@/services/funifier";

import Login from "./components/template/Login.vue";
import Dashboard from "./components/dashboard/Dashboard.vue";
import Producao from "./components/reports/Producao.vue";
import Produtividade from "./components/reports/Produtividade.vue";
import Jogadores from "./components/reports/Jogadores.vue";
import Metas from "./components/reports/Metas.vue";
import Nivel from "./components/reports/Nivel.vue";
import Ranking from "./components/reports/Ranking.vue";
import Produtos from "./components/reports/Produtos.vue";
import ProdutosFavoritos from "./components/reports/ProdutosFavoritos.vue";
import Importacao from "./components/import/Importacao.vue";
import Password from "./components/password/Password.vue";
import Clan from "./components/clan/Clan.vue";
import Erro from "./components/error/Erro.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "login",
      component: Login
    },
    {
      path: "/dashboard",
      name: "dashboard",
      component: Dashboard
    },
    {
      path: "/relatorio",
      name: "producao",
      component: Producao
    },
    {
      path: "/relatorio/produtividade",
      name: "produtividade",
      component: Produtividade
    },
    {
      path: "/relatorio/jogadores",
      name: "jogadores",
      component: Jogadores
    },
    {
      path: "/relatorio/metas",
      name: "metas",
      component: Metas
    },
    {
      path: "/relatorio/nivel",
      name: "nivel",
      component: Nivel
    },
    {
      path: "/relatorio/ranking",
      name: "ranking",
      component: Ranking
    },
    {
      path: "/relatorio/produtos",
      name: "produtos",
      component: Produtos
    },
    {
      path: "/relatorio/produtos-favoritos",
      name: "produtos-favoritos",
      component: ProdutosFavoritos
    },
    {
      path: "/importacao",
      name: "importacao",
      component: Importacao
    },
    {
      path: "/alterar-senha",
      name: "password",
      component: Password
    },
    {
      path: "/clan",
      name: "clan",
      component: Clan
    },
    {
      path: "/erro",
      name: "erro",
      component: Erro
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.path !== "/") {
    if (isAuth()) {
      next();
    } else {
      next({ path: "/" });
    }
  }
  next();
});

export default router;
