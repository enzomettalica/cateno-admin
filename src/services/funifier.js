import Axios from "axios";
import { baseApiUrl, apiKey } from "../global";

/**
 * Auth player by user name and password
 * @param {String} userName
 * @param {String} password
 */
export const auth = async (userName, password) => {
  try {
    const eApiKey = "apiKey=" + encodeURIComponent(apiKey);
    const eUserToPost = "&username=" + encodeURIComponent(userName);
    const ePassToPost = "&password=" + encodeURIComponent(password);
    const eGrantType = "&grant_type=" + encodeURIComponent("password");

    const req = await Axios({
      method: "POST",
      url: `${baseApiUrl}/v3/auth/token?${eApiKey}${eUserToPost}${ePassToPost}${eGrantType}`,
      headers: {
        "content-type": "application/json",
        "cache-control": "no-cache"
      }
    });

    const token = {
      token: "Bearer " + req.data.access_token,
      expires: req.data.expires_in
    };
    localStorage.setItem("funifier-admin", JSON.stringify(token));

    const hole = await getHole();
    if (hole && hole.includes("supervisor")) {
      let token = getToken();
      token.hole = "supervisor";
      token.userId = userName;
      localStorage.setItem("funifier-admin", JSON.stringify(token));
    } else if (hole && hole.includes("admin")) {
      let token = getToken();
      token.hole = "admin";
      token.userId = userName;
      localStorage.setItem("funifier-admin", JSON.stringify(token));
      return false;
    } else {
      const alert = {
        message: "Você não possui permissão para acessar esta página."
      };
      return alert;
    }

    return req;
  } catch (error) {
    console.error("ERROR:", error);
    return error;
  }
};

/**
 * Return a hole from a player
 * @param {null}
 */
export const getHole = async () => {
  try {
    let token = getToken();
    const req = await Axios({
      method: "GET",
      url: `${baseApiUrl}/v3/player/me/principal`,
      headers: {
        Authorization: token.token,
        "cache-control": "no-cache"
      }
    });
    return req.data.roles;
  } catch (error) {
    console.error("ERROR:", error);
    return error;
  }
};

/**
 * Return a token from localstorage
 * @param {null}
 */
export const getToken = () => {
  const hasToken = localStorage.getItem("funifier-admin")
    ? JSON.parse(localStorage.getItem("funifier-admin"))
    : null;
  if (hasToken) {
    return hasToken;
  } else {
    return false;
  }
};

/**
 * Check if player has hole
 * @param {string} hole
 */
export const hasHole = hole => {
  const token = getToken();
  if (token.hole === hole) {
    return true;
  } else {
    return false;
  }
};

/**
 * Check if player is auth
 * @param {null}
 */
export const isAuth = function() {
  const isTokenValid = function() {
    let currentTime = Date.now();
    var tokenExpire = localStorage.getItem("funifier-admin")
      ? JSON.parse(localStorage.getItem("funifier-admin"))
      : null;
    if (tokenExpire && tokenExpire.expires > currentTime) {
      return true;
    } else {
      return false;
    }
  };
  if (!isTokenValid()) {
    return false;
  } else {
    return true;
  }
};

/**
 * Get total of player registered
 * @param {null}
 */
export const getTotalPlayerRegistered = async () => {
  try {
    let token = getToken();
    const req = await Axios({
      method: "POST",
      url: `${baseApiUrl}/v3/database/player/aggregate`,
      headers: {
        Authorization: token.token,
        "cache-control": "no-cache"
      },
      data: [
        { $match: { "extra.status": { $exists: true } } },
        { $group: { _id: null, total: { $sum: 1 } } }
      ]
    });
    return req.data;
  } catch (error) {
    console.error("ERROR:", error);
    return error;
  }
};

/**
 * Check if player has hole
 * @param {string} startDate
 * @param {string} endDate
 * @param {boolean} oneDay
 */
export const getProductionByPlayer = async (startDate, endDate) => {
  let token = getToken();
  let sunrise = new Date(startDate);
  sunrise.setUTCHours(0);
  sunrise.setUTCMinutes(0);
  sunrise.setUTCSeconds(0);
  let sunset = new Date(endDate);
  sunset.setUTCHours(23);
  sunset.setUTCMinutes(59);
  sunset.setUTCSeconds(59);

  let query;
  if (token.hole === "admin") {
    query = [
      {
        $match: {
          actionId: "tratar",
          "attributes.time": {
            $gte: { $date: sunrise },
            $lte: { $date: sunset }
          }
        }
      },
      {
        $group: {
          _id: "$userId",
          total: { $sum: "$attributes.ocorrencias" },
          time: { $first: "$attributes.time" }
        }
      },
      {
        $lookup: {
          from: "player",
          localField: "_id",
          foreignField: "_id",
          as: "p"
        }
      },
      { $unwind: "$p" },
      {
        $project: {
          matricula: "$_id",
          total: 1,
          nome: "$p.name",
          teams: "$p.teams",
          meta: "$p.extra.meta",
          time: 1
        }
      },
      { $unwind: "$teams" },
      {
        $lookup: {
          from: "team",
          localField: "teams",
          foreignField: "_id",
          as: "team"
        }
      },
      { $unwind: "$team" },
      {
        $lookup: {
          from: "player",
          localField: "team.owner",
          foreignField: "_id",
          as: "supervisor"
        }
      },
      {
        $project: {
          matricula: "$_id",
          total: 1,
          nome: 1,
          teams: 1,
          meta: 1,
          supervisor: "$supervisor.name",
          matricula_supervisor: "$supervisor._id",
          time: 1
        }
      },
      { $unwind: "$matricula_supervisor" },
      { $unwind: "$supervisor" }
    ];
  }

  if (token.hole === "supervisor") {
    query = [
      {
        $match: {
          actionId: "tratar",
          "attributes.time": {
            $gte: { $date: sunrise },
            $lte: { $date: sunset }
          }
        }
      },
      {
        $group: {
          _id: "$userId",
          total: { $sum: "$attributes.ocorrencias" },
          time: { $first: "$attributes.time" }
        }
      },
      {
        $lookup: {
          from: "player",
          localField: "_id",
          foreignField: "_id",
          as: "p"
        }
      },
      { $unwind: "$p" },
      {
        $project: {
          matricula: "$_id",
          total: 1,
          nome: "$p.name",
          teams: "$p.teams",
          meta: "$p.extra.meta",
          time: 1
        }
      },
      { $unwind: "$teams" },
      {
        $lookup: {
          from: "team",
          localField: "teams",
          foreignField: "_id",
          as: "team"
        }
      },
      { $unwind: "$team" },
      {
        $lookup: {
          from: "player",
          localField: "team.owner",
          foreignField: "_id",
          as: "supervisor"
        }
      },
      {
        $project: {
          matricula: "$_id",
          total: 1,
          nome: 1,
          teams: 1,
          meta: 1,
          supervisor: "$supervisor.name",
          matricula_supervisor: "$supervisor._id",
          time: 1
        }
      },
      { $unwind: "$matricula_supervisor" },
      { $unwind: "$supervisor" },
      { $match: { matricula_supervisor: token.userId } }
    ];
  }
  try {
    const req = await Axios({
      method: "POST",
      url: `${baseApiUrl}/v3/database/action_log/aggregate?strict=true`,
      headers: {
        Authorization: token.token,
        "cache-control": "no-cache"
      },
      data: query
    });
    return req.data;
  } catch (error) {
    console.error("ERROR:", error);
    return error;
  }
};

export const getListOfUsers = async () => {
  let token = getToken();

  let query;
  if (token.hole === "admin") {
    query = [
      { $unwind: "$teams" },
      {
        $lookup: {
          from: "team",
          localField: "teams",
          foreignField: "_id",
          as: "team"
        }
      },
      { $unwind: "$team" },
      {
        $lookup: {
          from: "player",
          localField: "team.owner",
          foreignField: "_id",
          as: "supervisor"
        }
      },
      {
        $project: {
          _id: 1,
          name: 1,
          extra: 1,
          "team.name": 1,
          "supervisor.name": 1,
          "supervisor._id": 1
        }
      },
      { $unwind: "$supervisor" }
    ];
  }

  if (token.hole === "supervisor") {
    query = [
      { $unwind: "$teams" },
      {
        $lookup: {
          from: "team",
          localField: "teams",
          foreignField: "_id",
          as: "team"
        }
      },
      { $unwind: "$team" },
      {
        $lookup: {
          from: "player",
          localField: "team.owner",
          foreignField: "_id",
          as: "supervisor"
        }
      },
      {
        $project: {
          _id: 1,
          name: 1,
          extra: 1,
          "team.name": 1,
          "supervisor.name": 1,
          "supervisor._id": 1
        }
      },
      { $unwind: "$supervisor" },
      { $match: { "supervisor._id": token.userId } }
    ];
  }
  try {
    const req = await Axios({
      method: "POST",
      url: `${baseApiUrl}/v3/database/player/aggregate`,
      headers: {
        Authorization: token.token,
        "cache-control": "no-cache",
        Range: "itens=0-150"
      },
      data: query
    });
    return req.data;
  } catch (error) {
    console.error("ERROR:", error);
    return error;
  }
};

export const editPassword = async (name, playerId, password) => {
  let token = getToken();

  try {
    const req = await Axios({
      method: "PUT",
      url: `${baseApiUrl}/v3/database/registration__c`,
      headers: {
        Authorization: token.token,
        "cache-control": "no-cache"
      },
      data: {
        name: name,
        _id: playerId,
        password: password
      }
    });
    return req.data;
  } catch (error) {
    console.error("ERROR:", error);
    return error;
  }
};

export const getTeamByOwner = async ownerId => {
  let token = getToken();
  if (!ownerId) {
    ownerId = token.userId;
  }

  try {
    const req = await Axios({
      method: "GET",
      url: `${baseApiUrl}/v3/database/team?q=owner:'${ownerId}'`,
      headers: {
        Authorization: token.token,
        "cache-control": "no-cache"
      }
    });
    return req.data;
  } catch (error) {
    console.error("ERROR:", error);
    return error;
  }
};

export default {
  auth,
  getHole,
  getToken,
  hasHole,
  isAuth,
  getTotalPlayerRegistered,
  getProductionByPlayer,
  getListOfUsers,
  editPassword,
  getTeamByOwner
};
